const p1 = "Piu-piu";
const p2 = "Frajola";
var playTime = p1;
var gameOver = false;
		
VezDoJogador();
inicia();


function VezDoJogador(){

	if (gameOver){ 
		return;
	}

	if (playTime == p1) {

		var player = document.querySelectorAll("div#mostrador img")[0];
		player.setAttribute("src", "img/0.png");
	} else{

		var player = document.querySelectorAll("div#mostrador img")[0];
		player.setAttribute("src", "img/1.png");
	}
}

async function verificarVencedor(){


var a1 = document.getElementById("a1").getAttribute("jogada");
var a2 = document.getElementById("a2").getAttribute("jogada");
var a3 = document.getElementById("a3").getAttribute("jogada");

var b1 = document.getElementById("b1").getAttribute("jogada");
var b2 = document.getElementById("b2").getAttribute("jogada");
var b3 = document.getElementById("b3").getAttribute("jogada");

var c1 = document.getElementById("c1").getAttribute("jogada");
var c2 = document.getElementById("c2").getAttribute("jogada");
var c3 = document.getElementById("c3").getAttribute("jogada");


var vencedor = "";

if(((a1 == b1 && a1 == c1) || (a1 == a2 && a1 == a3 ) || (a1==b2 && a1 == c3 )) && a1 != "" ){
	vencedor = a1;

}else if((b2 == b1 && b2 == b3 && b2 !="" ) || (b2==a2 && b2==c2 && b2 !="") || (b2==a3 && b2==c1 && b2!="")){
	vencedor = b2;

}else if(((c3==c2 && c3==c1)||(c3==a3 && c3 == b3)) && c3 != ""){
	vencedor = c3;

}else if((a1 != null && a1 != 0) &&(a2 != null && a2 != 0) &&(a3 != null && a3 != 0) &&(b1 != null && b1 != 0) &&(b2 != null && b2 != 0) &&
(b3 != null && b3 != 0) &&(c1 != null && c1 != 0) &&(c2 != null && c2 != 0) &&(c3 != null && c3 != 0)){
	$("#resultado").html("<h1> Deu velha! </h1>");;
}

if (vencedor != "") {
	gameOver = true;
	$("#resultado").html("<h1>"+ vencedor + " venceu! </h1>");
}
}

function inicia(){

	var espacos = document.getElementsByClassName("espaco");
	for (var i = 0; i < espacos.length; i++) {

		espacos[i].addEventListener("click", function(){

			if (gameOver) {return;}

			if(this.getElementsByTagName("img").length == 0){

				if (playTime == p1) {
					this.innerHTML = "<img src='img/0.png' border='0'>";
					this.setAttribute("jogada", p1);
					playTime = p2;

				}else{
					this.innerHTML = "<img src='img/1.png' border='0'>";
					this.setAttribute("jogada", p2);
					playTime = p1;
				}
				VezDoJogador();
				verificarVencedor();

			}

		});
	}
}